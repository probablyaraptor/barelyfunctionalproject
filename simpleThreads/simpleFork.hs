import Control.Concurrent
import Data.ByteString.Char8 as Char8

-- Prints a value in a threaded monad
forkPrintVal x = forkIO $ do
    safePrintInt x

-- Prints an integer without having lazy evaluation mess with the end result.
safePrintInt :: Int -> IO()
safePrintInt = Char8.putStrLn . Char8.pack . show

-- Prints 1 -> 20 in different threads
main = do
    mapM_ forkPrintVal [1..20]