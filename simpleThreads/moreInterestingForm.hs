import Control.Concurrent.Async
import Data.ByteString.Char8 as Char8

{-| 
    Interestingly I started playing around with the, arguably,
    cleaner version of this in the last file, and found that I
    could reliably get more random results by sticking more
    methods into the function we concurrently run. Same effect,
    but less likey to come out in perfect order.
-}

-- This puts the number into an 8 byte character string
threadSafePrinter number = Char8.putStrLn
    $ Char8.pack
    $ show number

main = do
    mapConcurrently_ threadSafePrinter  [1..20]