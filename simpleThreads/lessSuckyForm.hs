import Control.Concurrent.Async
import Data.ByteString.Char8 as Char8

{-| 
    After digging deeper I found a simpler way to set off
    a number of threads, and found out that my second thought,
    that laziness was ruining our print was correct. In fact
    you can't trust any printing with strings ins haskell
    as they are just lazily evaluated arrays of characters.
    Below is a working version of my last attempt.
-}

-- This puts the number into an 8 byte character string
packageNumber :: Int -> ByteString
packageNumber = Char8.pack . show 

main = do
    mapConcurrently_ (Char8.putStrLn . packageNumber)  [1..20]