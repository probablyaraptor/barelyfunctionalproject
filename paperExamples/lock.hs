import Control.Concurrent
import Control.Concurrent.Async
import Data.ByteString.Char8 as Char8

safePrint = Char8.putStrLn . Char8.pack  

-- Define a lock
type Lock = MVar ()
newLock = newMVar ()
lockWith f = withMVar f . const

-- Print with a lock
lockedPrint lock = (lockWith lock) . safePrint

-- Creates a lock and then prints 1->20 on different threads using the lock.
main = do
    lock <- newLock
    mapConcurrently_ ((lockedPrint lock) . show) [1..20]