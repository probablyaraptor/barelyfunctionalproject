import Control.Concurrent
import Control.Concurrent.Async
import Data.ByteString.Char8 as Char8

safePrint = Char8.putStrLn . Char8.pack  

-- Define a type which acts like a barrier
type Barrier a = MVar a
newBarrier = newEmptyMVar
tripBarrier = putMVar
waitAtBarrier = readMVar

-- Function to call when the barrier is tripped.
finishRacer = safePrint . show

-- Creates a barrier. Sets up an thread which wants to print the
-- return value of the barrier, waiting at the barrier for the value.
-- Prints a number of lines in the main thread, to prove the other thread
-- is in fact waiting. Trips the barrier with a zero, then waits on the 
-- thread's handle before printing one final time and closing.
main = do
    barrier <- newBarrier
    racerHandle <- async $ do
        val <- waitAtBarrier barrier
        finishRacer val
    print "On your marks..."
    print "Get set..."
    print "Go!"
    tripBarrier barrier 0
    wait racerHandle
    print "Done!"
