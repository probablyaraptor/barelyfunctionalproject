import Control.Concurrent
import Control.Concurrent.Async
import Data.ByteString.Char8 as Char8

safePrint = Char8.putStrLn . Char8.pack  

-- Define a mutex type
type Mutex = MVar ()
newMutex = newMVar ()
claimMutex = takeMVar
releaseMutex m = putMVar m ()

-- Print in the middle of a mutex lock
mutPrint mutex value = do
    claimMutex mutex
    safePrint value
    releaseMutex mutex

-- Create a mutex, then use it to print 1 -> 20 concurrently.
main = do
    mutex <- newMutex
    mapConcurrently_ ((mutPrint mutex) . show) [1..20]