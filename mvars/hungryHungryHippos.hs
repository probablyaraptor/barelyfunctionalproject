import Control.Concurrent.Async
import Control.Concurrent
import Data.ByteString.Char8 as Char8

{-| This is a different take on the batton program, where
    I just setup a number of consumers and throw out bait
    until they've all been fed.
-}

safePrint :: Int -> IO()
safePrint = Char8.putStrLn . Char8.pack . show 

feedHippo sharedVar val = forkIO $ do 
    putMVar sharedVar val

createHippo var id = do
    value <- takeMVar var
    safePrint id
    
main = do
    sharedVar <- newMVar 0
    mapM_ (feedHippo sharedVar) [1..20]
    mapConcurrently_ (createHippo sharedVar) [1..20]
