import Control.Concurrent.Async
import Control.Concurrent
import Data.ByteString.Char8 as Char8

{-| This program simply takes a set of threads, labeled
    1-20, and offers a piece of shared memory for them to
    compete over. One thread will get the memory, then it will
    write it's ID number to the console and add 1 to the memory.
    This lets the memory act as a counter so the last thread
    to catch doesn't lock the program open by putting values
    back in the shared memory.

    This is using the MVar to act as a sort of pipeline for
    memory while modeling the idea of a person throwing a
    baton into the air for it to be caught, then leaving 
    the group. All group members will eventually, and 
    non-deterministically catch the baton and call out
    their id.
-}

safePrint :: Int -> IO()
safePrint = Char8.putStrLn . Char8.pack . show 

passBaton max var count
    | count == max  = return ()
    | otherwise = putMVar var (count+1)

work var max id = do
    value <- takeMVar var
    safePrint id
    passBaton max var value
    
main = do
    sharedVar <- newMVar 1
    mapConcurrently_ (work sharedVar 20) [1..20]